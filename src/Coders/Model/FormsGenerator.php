<?php
namespace Reliese\Coders\Model;

use Illuminate\Support\Arr;
use Illuminate\Support\Fluent;
use Illuminate\Support\Str;

class FormsGenerator
{
    const CUSTOM_RULE_TYPE_EMAIL = 'email';
    const CUSTOM_RULE_TYPE_ARRAY = 'array';
    const CUSTOM_RULE_TYPE_URL = 'url';
    const CUSTOM_RULE_TYPE_NOT_REQUIRED = 'not_required';

    const CUSTOM_FILTER_TYPE_STRING_TO_UPPER = 'strtoupper';
    const CUSTOM_FILTER_TYPE_STRING_TO_LOWER = 'strtolower';
    const FILTER_TYPE_STRING_TRIM = 'trim';

    const INPUT_TEXT = 'text';
    const INPUT_TEXT_AREA = 'textarea';
    const INPUT_SELECT = 'select';
    const INPUT_DATE = 'date';
    const INPUT_DATETIME = 'datetime';
    const INPUT_INT = 'int';
    const INPUT_DOUBLE = 'double';
    const INPUT_BOOLEAN = 'boolean';
    const INPUT_RELATION = 'relation';
    const INPUT_EMAIL = 'email';


    /**
     * @var Model
     */
    protected $model;

    /**
     * @var array
     */
    protected $clientRules = [];

    /**
     * @var array
     */
    protected $rules = [];

    /**
     * @var array
     */
    protected $filters = [];

    /**
     * @var array
     */
    protected $inputs = [];


    /**
     * @var array
     */
    protected $patterns = [];

    /**
     * @var array
     */
    protected $filterPatterns = [];

    /**
     * @var array
     */
    protected $lookUp = [];

    /**
     * @var array
     */
    protected $relations = [];

    /**
     * @param Model $model
     * @param array $patterns
     * @param array $filterPatterns
     * @param array $lookUp
     */
    public function generate(Model $model, $patterns = [], $filterPatterns = [], $lookUp = [])
    {
        $this->model = $model;
        $this->patterns = $patterns;
        $this->filterPatterns = $filterPatterns;
        $this->lookUp = $lookUp;
        $this->clientRules = [];
        $this->rules = [];
        $this->filters = [];
        $this->inputs = [];
        $this->relations = [];
        $this->generateAllRules();
        $this->generateCustomFilters();
        $this->generateInputs();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return $this->formatRules();
    }

    /**
     * @return array
     */
    public function clientRules()
    {
        return $this->formatClientRules();
    }

    /**
     * @return array
     */
    public function filters()
    {
        return $this->formatFilters();
    }

    /**
     * @return array
     */
    public function inputs()
    {
        return $this->inputs;
    }

    /**
     *
     */
    protected function generateAllRules()
    {
        $connectionName = $this->model->getConnectionName();
        $tableName = $this->model->getBlueprint()->table();

        foreach ($this->model->getBlueprint()->columns() as $column) {
            $this->generateRules($column);
        }

        $this->generateUniqueRule(
            "{$connectionName}.{$tableName}",
            $this->model->getBlueprint()->primaryKey()
        );

        foreach ($this->model->getBlueprint()->uniques() as $unique) {
            $this->generateUniqueRule("{$connectionName}.{$tableName}", $unique);
        }

        $this->generateFKRule(
            $this->model->getBlueprint()->relations(),
            $this->model->getRelations()
        );
    }

    /**
     * @param Fluent $column
     */
    protected function generateRules(Fluent $column)
    {
        $columnName = $column->get('name');
        if ($this->isRequired($column)) {
            $this->addRequireRule($columnName);
        }

        switch (strtolower($column->get('type'))) {
            case 'string' :
                $this->addStringRule($columnName, $column);
                break;
            case 'bool' :
                $this->addBooleanRule($columnName);
                break;
            case 'date' :
                $this->addDateRule($columnName, $column);
                break;
            case 'int' :
                $this->addIntRule($columnName);
                break;
            case 'float' :
                $this->addFloatRule($columnName, $column);
                break;
        }

        if ($this->requireCustomRule(static::CUSTOM_RULE_TYPE_ARRAY, $columnName)) {
            $this->addArrayRule($columnName);
        }

        if ($this->requireCustomRule(static::CUSTOM_RULE_TYPE_EMAIL, $columnName)) {
            $this->addEmailRule($columnName);
        }

        if ($this->requireCustomRule(static::CUSTOM_RULE_TYPE_URL, $columnName)) {
            $this->addUrlRule($columnName);
        }

        $this->generateEnumRule($columnName, $column);
    }

    /**
     * @param Fluent $column
     * @return bool
     */
    protected function isRequired(Fluent $column)
    {
        $null = $column->get('nullable');
        $autoincrement = $column->get('autoincrement');
        $hasNotRequiredRule = $this->requireCustomRule(
            static::CUSTOM_RULE_TYPE_NOT_REQUIRED,
            $column->get('name')
        );

        return !$null && !$autoincrement && !$hasNotRequiredRule;
    }

    /**
     * @param array $rules
     * @return array
     */
    protected function formatRules($rules = [])
    {
        $rules = empty($rules) ? $this->rules : $rules;
        return array_map(function ($val) {
            return implode('|', $val);
        }, $rules);
    }

    /**
     * @return array
     */
    protected function formatClientRules()
    {
        if (empty($this->clientRules)) {
            return [];
        }
        return $this->formatRules($this->clientRules);
    }

    /**
     * @param $tableName
     * @param $columns
     * @param $references
     */
    protected function generateFkRuleByTable($tableName, $columns, $references)
    {
        $fieldName = array_shift($columns);
        $reference = array_shift($references);

        if (count($columns) == 0) {
            $rule = "exists:{$tableName}";
            $rule = ($fieldName == $reference) ? $rule : $rule . ",{$reference}";
            $this->addRule($fieldName, $rule);
            return;
        }

        $extra = ($fieldName == $reference) ? [] : ["{$fieldName}={$reference}"];
        for ($i = 0; $i < count($columns); $i++) {
            $extraField = $columns[$i];
            $fieldReference = $references[$i];
            $extra[] = ($extraField == $fieldReference) ? $extraField : "{$extraField}={$fieldReference}";
        }
        $extra = implode(',', $extra);
        $rule = "exists_with:{$tableName},{$extra}";
        $this->addRule($fieldName, $rule);
    }

    /**
     * @param $rule
     * @param $attribute
     * @return bool
     */
    protected function requireCustomRule($rule, $attribute)
    {
        if (!isset($this->patterns[$rule])) {
            return false;
        }
        $patterns = $this->patterns[$rule];
        foreach ($patterns as $pattern) {
            if (preg_match("/$pattern/", $attribute)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $columnName
     * @param Fluent $column
     */
    protected function generateEnumRule($columnName, Fluent $column)
    {
        $values = $this->enumValues($column);
        if (empty($values)) {
            return;
        }
        $values = implode(',', $values);
        $this->addClientRule($columnName, "in:{$values}");
    }

    /**
     * @param $tableName
     * @param Fluent $pk
     */
    protected function generateUniqueRule($tableName, Fluent $pk)
    {
        $columns = $pk->get('columns', []);
        if (count($columns) == 0) {
            return;
        }
        $columnName = array_shift($columns);
        if (count($columns) == 0) {
            $rule = "unique:{$tableName}";
        } else {
            $fields = implode(',', $columns);
            $rule = "unique_with:{$tableName},{$fields}";
        }

        $this->addRule($columnName, $rule);
    }

    /**
     * @param Fluent[] $columns
     * @param Relation[] $relations
     */
    protected function generateFKRule($columns, $relations)
    {
        if (empty($relations)) {
            return;
        }

        /* @var $column Fluent */
        foreach ($columns as $column) {
            $tableName = $column->get('on', []);
            if (!isset($tableName['table'])) {
                continue;
            }
            $tableName = $tableName['table'];

            $connectionName = $this->searchConnectionName($relations, $tableName);

            if (empty($connectionName)) {
                continue;
            }

            $columnsTable = $column->get('columns', []);
            $references = $column->get('references', []);
            if (count($columnsTable) != count($references)) {
                continue;
            }

            $this->saveRelations($tableName, $columnsTable, $references);
            $this->generateFkRuleByTable("{$connectionName}.{$tableName}", $columnsTable, $references);
        }
    }

    /**
     * @param $column
     * @param $rule
     */
    protected function addRule($column, $rule)
    {
        if (!isset($this->rules[$column])) {
            $this->rules[$column] = [];
        }
        $this->rules[$column][] = $rule;
    }

    /**
     * @param $columnName
     * @param $rule
     * @param null $column
     */
    protected function addClientRule($columnName, $rule, $column = null)
    {
        if (!isset($this->clientRules[$columnName])) {
            $this->clientRules[$columnName] = [];
        }
        $clientRule = $this->serverRuleToClientRule($rule, $column);
        if (!empty($clientRule)) {
            $this->clientRules[$columnName][] = $clientRule;
        }
        $this->addRule($columnName, $rule);
    }

    /**
     * @param $column
     * @param $rule
     */
    protected function serverRuleToClientRule($rule, $column = null)
    {
        switch ($rule) {
            case 'string':
                return '';
            case 'boolean':
                return 'min_value:0|max_value:1';
        }

        if (Str::startsWith($rule, 'in:')) {
            return str_replace('in:', 'oneOf:', $rule);
        }

        if ($rule == 'date' && $column) {
            $format = $column->get('subType', '') === 'datetime' ? 'YYYY-MM-DD HH:mm:ss' : 'YYYY-MM-DD';

            return "date_format:{$format}";
        }


        return $rule;
    }

    /**
     * @param $columnName
     * @param Fluent $column
     */
    protected function addStringRule($columnName, Fluent $column)
    {
        $this->addClientRule($columnName, 'string');
        $this->addFilter($columnName, static::FILTER_TYPE_STRING_TRIM);
        $max = $column->get('size');
        if (is_numeric($max)) {
            $this->addClientRule($columnName, "max:{$max}");
        }
    }

    /**
     * @param $columnName
     */
    protected function addBooleanRule($columnName)
    {
        $this->addClientRule($columnName, 'boolean');
    }

    /**
     * @param $columnName
     */
    protected function addRequireRule($columnName)
    {
        $this->addClientRule($columnName, 'required');
    }

    /**
     * @param $columnName
     * @param Fluent $column
     */
    protected function addDateRule($columnName, Fluent $column)
    {
        $this->addClientRule($columnName, 'date', $column);
    }

    /**
     * @param $columnName
     */
    protected function addIntRule($columnName)
    {
        $this->addClientRule($columnName, 'integer');
    }

    /**
     * @param $columnName
     * @param Fluent $column
     */
    protected function addFloatRule($columnName, $column)
    {
        $int = $column->get('size');
        $decimals = $column->get('scale');
        $int = $int - $decimals;
        $int = str_repeat('9', $int);
        $decimals = str_repeat('9', $decimals);

        $this->addClientRule($columnName, "between:0,{$int}.{$decimals}");
    }

    /**
     * @param $columnName
     */
    protected function addEmailRule($columnName)
    {
        $this->addClientRule($columnName, static::CUSTOM_RULE_TYPE_EMAIL);
    }

    /**
     * @param $columnName
     */
    protected function addUrlRule($columnName)
    {
        $this->addClientRule($columnName, static::CUSTOM_RULE_TYPE_URL);
    }

    /**
     * @param $columnName
     */
    protected function addArrayRule($columnName)
    {
        $this->addClientRule($columnName, static::CUSTOM_RULE_TYPE_ARRAY);
    }

    /**
     * @param Relation[] $relations
     * @param $tableName
     * @return string
     */
    protected function searchConnectionName($relations, $tableName)
    {
        if (isset($this->lookUp[$tableName])) {
            return $this->lookUp[$tableName];
        }
        foreach ($relations as $relation) {
            if ($relation->tableName() == $tableName) {
                return $relation->connectionName();
            }
        }
        return '';
    }

    /**
     *
     */
    protected function generateCustomFilters()
    {
        foreach ($this->model->getBlueprint()->columns() as $column) {
            $columnName = $column->get('name');
            if ($this->requireCustomFilter(static::CUSTOM_FILTER_TYPE_STRING_TO_LOWER, $columnName)) {
                $this->addFilter($columnName, static::CUSTOM_FILTER_TYPE_STRING_TO_LOWER);
            }
            if ($this->requireCustomFilter(static::CUSTOM_FILTER_TYPE_STRING_TO_UPPER, $columnName)) {
                $this->addFilter($columnName, static::CUSTOM_FILTER_TYPE_STRING_TO_UPPER);
            }
        }
    }

    /**
     *
     */
    protected function generateInputs()
    {
        foreach ($this->model->getBlueprint()->columns() as $column) {
            $this->addInput($column);
        }
    }

    /**
     * @param $filter
     * @param $attribute
     * @return bool
     */
    protected function requireCustomFilter($filter, $attribute)
    {
        if (!isset($this->patterns[$filter])) {
            return false;
        }
        $patterns = $this->patterns[$filter];
        foreach ($patterns as $pattern) {
            if (preg_match("/$pattern/", $attribute)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $column
     * @param $filter
     */
    protected function addFilter($column, $filter)
    {
        if (!isset($this->filters[$column])) {
            $this->filters[$column] = [];
        }
        $this->filters[$column][] = $filter;
    }

    /**
     * @param $column
     * @return array|void
     */
    protected function addInput($column)
    {
        $columnName = $column->get('name');
        $input = $this->formatInput($columnName, $column);

        if (!$input) {
            return;
        }

        return $this->inputs[$columnName] = $input;
    }

    /**
     * @return array
     */
    protected function formatFilters()
    {
        if (empty($this->filters)) {
            return [];
        }
        return array_map(function ($val) {
            return implode('|', $val);
        }, $this->filters);
    }

    /**
     * @param $columnName
     * @param Fluent $column
     * @return array
     */
    protected function formatInput($columnName, Fluent $column)
    {
        $relation = Arr::get($this->relations, $columnName);
        if ($relation) {
            return $this->formatRelationInput($column, $relation);
        }

        $input = $this->formatEnumInput($column);
        if ($input) {
            return $input;
        }

        switch (strtolower($column->get('type'))) {
            case 'string' :
                return $this->formatStringInput($columnName, $column);
            case 'bool' :
                return $this->formatBooleanInput($column);
            case 'date' :
                return $this->formatDateInput($column);
            case 'int' :
                return $this->formatIntInput($column);
            case 'float' :
                return $this->formatFloatInput($column);
        }
    }

    /**
     * @param Fluent $column
     * @return array
     */
    protected function formatStringInput($columnName, Fluent $column)
    {
        $max = $column->get('size');
        if (!is_numeric($max)) {
            $max = 150;
        }

        if ($this->requireCustomRule(static::CUSTOM_RULE_TYPE_EMAIL, $columnName)) {
            $type = static::INPUT_EMAIL;
        } else if ($max < 200) {
            $type = static::INPUT_TEXT;
        } else {
            $type = static::INPUT_TEXT_AREA;
        }

        return [
            'type' => $type,
            'default' => $this->defaultColumnValue($column),
        ];
    }

    /**
     * @param Fluent $column
     * @return array
     */
    protected function formatBooleanInput(Fluent $column)
    {
        return [
            'type' => static::INPUT_BOOLEAN,
            'default' => $this->defaultColumnValue($column),
        ];
    }

    /**
     * @param Fluent $column
     * @return array
     */
    protected function formatRelationInput(Fluent $column, $relation)
    {
        return array_merge([
            'type' => static::INPUT_RELATION,
            'default' => $this->defaultColumnValue($column),
        ], $relation);
    }

    /**
     * @param Fluent $column
     * @return array
     */
    protected function formatEnumInput(Fluent $column)
    {
        $values = $this->enumValues($column);
        if (empty($values)) {
            return null;
        }

        return [
            'type' => static::INPUT_SELECT,
            'options' => $values,
            'default' => $this->defaultColumnValue($column),
        ];
    }

    /**
     * @param Fluent $column
     * @return array
     */
    protected function formatDateInput(Fluent $column)
    {
        $isDatetime = $column->get('subType', '') === 'datetime';
        return [
            'type' => $isDatetime ? static::INPUT_DATETIME : static::INPUT_DATE,
            'format' => $isDatetime ? 'YYYY-MM-DD HH:mm:ss' : 'YYYY-MM-DD',
            'default' => $this->defaultColumnValue($column),
        ];
    }

    /**
     * @param Fluent $column
     * @return array
     */
    protected function formatIntInput(Fluent $column)
    {
        return [
            'type' => static::INPUT_INT,
            'default' => $this->defaultColumnValue($column),
        ];
    }

    /**
     * @param Fluent $column
     * @return array
     */
    protected function formatFloatInput(Fluent $column)
    {
        return [
            'type' => static::INPUT_DOUBLE,
            'default' => $this->defaultColumnValue($column),
        ];
    }

    /**
     * @param Fluent $column
     * @return mixed
     */
    protected function defaultColumnValue(Fluent $column)
    {
        return $column->get('default');
    }

    /**
     * @param Fluent $column
     * @return array
     */
    protected function enumValues(Fluent $column)
    {
        return $column->get('enum', []);
    }

    /**
     * @param $tableName
     * @param $columns
     * @param $references
     */
    protected function saveRelations($tableName, $columns, $references)
    {
        $model = Str::kebab(Str::singular($tableName));
        foreach ($columns as $k => $column) {
            $this->relations[$column] = [
                'key' => $references[$k],
                'model' => $model,
            ];
        }
    }
}