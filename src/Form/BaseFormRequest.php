<?php
namespace Reliese\Form;

use Illuminate\Foundation\Http\FormRequest as BaseFormRequest;
use Illuminate\Support\Arr;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class FormRequest extends BaseFormRequest
{
    use SanitizesInput;

    /**
     * @return array
     */
    public function clientRules()
    {
        return [];
    }

    /**
     * @return array
     */
    protected function inputs()
    {
        return [];
    }

    /**
     * @return array
     */
    public function form()
    {
        $rules = $this->clientRules();
        $form = [];
        foreach ($this->inputs() as $attribute => $input) {
            $input['rules'] = Arr::get($rules, $attribute,  '');
            $form[$attribute] = $input;
        }

        return $form;
    }

    public function columns()
    {
        $columns = [];
        foreach ($this->inputs() as $attribute => $input) {
            $columns[$attribute] = [
                'type' => $input['type'],
                'sortable' => true,
            ];
        }

        return $columns;
    }
}