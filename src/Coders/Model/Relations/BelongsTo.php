<?php

/**
 * Created by Cristian.
 * Date: 05/09/16 11:41 PM.
 */
namespace Reliese\Coders\Model\Relations;

use Illuminate\Support\Fluent;
use Illuminate\Support\Str;
use Reliese\Coders\Model\Model;
use Reliese\Coders\Model\Relation;
use Reliese\Support\Dumper;

class BelongsTo implements Relation
{
    /**
     * @var \Illuminate\Support\Fluent
     */
    protected $command;

    /**
     * @var \Reliese\Coders\Model\Model
     */
    protected $parent;

    /**
     * @var \Reliese\Coders\Model\Model
     */
    protected $related;

    /**
     * BelongsToWriter constructor.
     *
     * @param \Illuminate\Support\Fluent $command
     * @param \Reliese\Coders\Model\Model $parent
     * @param \Reliese\Coders\Model\Model $related
     */
    public function __construct(Fluent $command, Model $parent, Model $related)
    {
        $this->command = $command;
        $this->parent = $parent;
        $this->related = $related;
    }

    /**
     * @return string
     */
    public function name()
    {
        if ($this->parent->usesSnakeAttributes()) {
            return Str::snake($this->related->getClassName());
        }

        return Str::camel($this->related->getClassName());
    }

    /**
     * @return string
     */
    public function body()
    {
        $body = 'return $this->belongsTo(';

        $body .= $this->related->getQualifiedUserClassName().'::class';

        if ($this->needsForeignKey()) {
            $data = $this->hasCompositeOtherKey() ?
                Dumper::export(array_values($this->command->columns), 4) :
                Dumper::export($this->foreignKey())
            ;
            $body .= ', ' . $data;
        }

        if ($this->needsOtherKey()) {
            $data = $this->hasCompositeOtherKey() ?
                Dumper::export(array_values($this->command->references), 4) :
                Dumper::export($this->otherKey())
            ;
            $body .= ', ' . $data;
        }

        $body .= ');';

        return $body;
    }

    /**
     * @return string
     */
    public function hint()
    {
        return $this->related->getQualifiedUserClassName();
    }

    /**
     * @return bool
     */
    protected function needsForeignKey()
    {
        $defaultForeignKey = $this->related->getRecordName().'_id';

        return $defaultForeignKey != $this->foreignKey() || $this->needsOtherKey();
    }

    /**
     * @param int $index
     *
     * @return string
     */
    protected function foreignKey($index = 0)
    {
        return $this->command->columns[$index];
    }

    /**
     * @param int $index
     *
     * @return string
     */
    protected function qualifiedForeignKey($index = 0)
    {
        return $this->parent->getTable().'.'.$this->foreignKey($index);
    }

    /**
     * @return bool
     */
    protected function needsOtherKey()
    {
        $defaultOtherKey = $this->related->getPrimaryKey();

        return $defaultOtherKey != $this->otherKey();
    }

    /**
     * @param int $index
     *
     * @return string
     */
    protected function otherKey($index = 0)
    {
        return $this->command->references[$index];
    }

    /**
     * @param int $index
     *
     * @return string
     */
    protected function qualifiedOtherKey($index = 0)
    {
        return $this->related->getTable().'.'.$this->otherKey($index);
    }

    /**
     * Whether the "other key" is a composite foreign key.
     *
     * @return bool
     */
    protected function hasCompositeOtherKey()
    {
        return count($this->command->references) > 1;
    }

    /**
     * @return string
     */
    public function connectionName()
    {
        return $this->related->getConnectionName();
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return $this->related->getTable();
    }

    /**
     * @return []
     */
    public function info()
    {
        $keys = [];
        foreach ($this->command->references as $index => $column) {
            $keys[$this->otherKey($index)] = $this->foreignKey($index);
        }
        return [
            'type'  => 'belongs-to',
            'model' => $this->related->getName(),
            'keys'  => $keys
        ];
    }
}
